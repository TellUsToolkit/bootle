
// ================================================================================
//  University of Manchester. UK.
//  School of Environment, Education, and Development.
//  Centre for Urban Policy Studies.
// 
//  Name:            Enquiry [/models]
//  Original coding: Vasilis Vlastaras (@gisvlasta), 25/04/2016.
// 
//  Description:     Used to submit an enquiry.
// ================================================================================


var keystone = require('keystone');
var Types = keystone.Field.Types;

// Create the Enquiry.
var Enquiry = new keystone.List('Enquiry', {
  nocreate: true,
  noedit: true
});

// Add fields in the model.
Enquiry.add({
  
  // The name of the pesron submitting the enquiry.
  name: { type: Types.Name, required: true },
  
  // The description of the model.
  email: { type: Types.Email, required: true },
  
  // The phone of the person sending the enquiry.
  phone: { type: String },
  
  // The type of enquiry.
  enquiryType: { type: Types.Select, options: [
    { value: 'message', label: 'Just leaving a message' },
    { value: 'question', label: 'I\'ve got a question' },
    { value: 'other', label: 'Something else...' }
  ] },
  
  // The message of the enquiry.
  message: { type: Types.Markdown, required: true },
  
  // The date that this enquiry was created.
  createdAt: { type: Date, default: Date.now }
  
});

Enquiry.schema.pre('save', function(next) {
  this.wasNew = this.isNew;
  next();
});

Enquiry.schema.post('save', function() {
  if (this.wasNew) {
    this.sendNotificationEmail();
  }
});

Enquiry.schema.methods.sendNotificationEmail = function(callback) {
  
  if ('function' !== typeof callback) {
    callback = function() {};
  }
  
  var enquiry = this;
  
  keystone.list('User').model.find().where('isAdmin', true).exec(function(err, admins) {
    
    if (err) return callback(err);
    
    new keystone.Email('enquiry-notification').send({
      to: admins,
      from: {
        name: 'Bootle',
        email: 'contact@bootle.com'
      },
      subject: 'New Enquiry for Bootle',
      enquiry: enquiry
    }, callback);
    
  });
  
};

// Register the model.
Enquiry.defaultSort = '-createdAt';
Enquiry.defaultColumns = 'name, email, enquiryType, createdAt';
Enquiry.register();
