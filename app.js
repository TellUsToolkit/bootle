// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone');
var handlebars = require('express-handlebars');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.
keystone.init({

  'name': 'Bootle',
  'brand': 'Bootle',
  
  'less': 'public',
  'static': 'public',
  'favicon': 'public/favicon.ico',
  'views': 'templates/views',
  'view engine': 'hbs',
  
  'custom engine': handlebars.create({
    layoutsDir: 'templates/views/layouts',
    partialsDir: 'templates/views/partials',
    defaultLayout: 'default',
    helpers: new require('./templates/views/helpers')(),
    extname: '.hbs'
  }).engine,
  
  'emails': 'templates/emails',
  
  'auto update': true,
  'session': true,
  'auth': true,
  'user model': 'User',
  'port': 8084,
  'signin url': '/signin',
  'signin redirect': '/',
  'signout url': '/signout',
  'signout redirect': '/',
  //'signin logo': ['/images/logos/bootle-2020-logo.svg', 200, 200]
  'signin logo': ['/images/key.svg', 200, 200],
  //'signin logo': ['/images/key3.svg', 200, 200],
  //'signin logo': ['/images/user3.svg', 200, 200],
  //'signin logo': ['/images/users2.svg', 200, 200],
  //'signin logo': ['/images/users4.svg', 200, 200]
  
});

// Load your project's Models
keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
  _: require('underscore'),
  env: keystone.get('env'),
  utils: keystone.utils,
  editable: keystone.content.editable
});

// Load your project's Routes
keystone.set('routes', require('./routes'));


// Setup common locals for your emails. The following are required by Keystone's
// default email templates, you may remove them if you're using your own.
keystone.set('email locals', {
  logo_src: '/images/logo-email.gif',
  logo_width: 194,
  logo_height: 76,
  theme: {
    email_bg: '#f9f9f9',
    link_color: '#2697de',
    buttons: {
      color: '#fff',
      background_color: '#2697de',
      border_color: '#1a7cb7'
    }
  }
});

// Setup replacement rules for emails, to automate the handling of differences
// between development a production.

// Be sure to update this rule to include your site's actual domain, and add
// other rules your email templates require.
keystone.set('email rules', [{
  find: '/images/',
  replace: (keystone.get('env') == 'production') ? 'http://www.your-server.com/images/' : 'http://localhost:8084/images/'
}, {
  find: '/keystone/',
  replace: (keystone.get('env') == 'production') ? 'http://www.your-server.com/keystone/' : 'http://localhost:8084/keystone/'
}]);

// Load your project's email test routes
keystone.set('email tests', require('./routes/emails'));

// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
  'enquiries': 'enquiries',
  'galleries': 'galleries',
  'geolayers': ['geo-layers', 'geo-layer-definitions', 'popup-functions', 'symbology-functions'],
  'plans': ['comments', 'files', 'plans', 'plan-texts'],
  'posts': ['posts', 'post-categories'],
  'styles': ['base-icon-styles', 'circle-marker-styles', 'div-icon-styles', 'fill-styles', 'icon-styles', 'line-styles', 'simple-fill-styles'],
  'users': 'users'
});

// Start Keystone to connect to your database and initialise the web server
keystone.start();
