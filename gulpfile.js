var gulp = require('gulp');
var jshint = require('gulp-jshint');
var jshintReporter = require('jshint-stylish');
//var eslint = require('gulp-eslint');
var watch = require('gulp-watch');
var shell = require('gulp-shell')


var paths = {
  'src':['./models/**/*.js','./routes/**/*.js', 'app.js', 'package.json']

};

// gulp lint
gulp.task('lint', function(){
  gulp.src(paths.src)
    .pipe(jshint())
    .pipe(jshint.reporter(jshintReporter));
});

// gulp lint
// gulp.task('lint', function() {
//   gulp.src(paths.src)
//     .pipe(eslint())
//     //.pipe(eslint.format())
//     // Brick on failure to be super strict
//     //.pipe(eslint.failOnError());    
// });

// gulp watcher for lint
gulp.task('watch:lint', function () {
  gulp.src(paths.src)
    .pipe(watch())
    .pipe(jshint())
    .pipe(jshint.reporter(jshintReporter));
});

gulp.task('runKeystone', shell.task('node app.js'));
gulp.task('watch', [

  'watch:lint'
]);

gulp.task('default', ['watch', 'runKeystone']);
