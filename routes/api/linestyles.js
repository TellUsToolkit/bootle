
// ================================================================================
//  University of Manchester. UK.
//  School of Environment, Education, and Development.
//  Centre for Urban Policy Studies.
// 
//  Name:            linestyles.js [/routes/api]
//  Original coding: Vasilis Vlastaras (@gisvlasta), 04/05/2016.
// 
//  Description:     Defines the functions mapped to the REST API endpoints
//                   to deal with linestyles.
// ================================================================================

var async = require('async');
var keystone = require('keystone');
var bootle = require('../../bootle');

var LineStyle = keystone.list('LineStyle');

// ================================================================================
//  Public Methods

/**
 * List LineStyles. [ /api/linestyles ]
 */
exports.list = function(req, res) {
  
  LineStyle.model.find(function(error, items) {
    
    // Check if an error has occured.
    if (error != null) {
      if (error) {
        var detail = { message: bootle.errors.itemsRetrievalError.message, internalError: { name: error.name, message: error.message } };
        
        return res.apiError(bootle.errors.itemsRetrievalError.code, detail, error, bootle.httpStatus.internalServerError.code);
      }
    }
    
    // Check if items have been found.
    if (!items) {
      return res.apiError(bootle.errors.itemsNotFound.code, { message: bootle.errors.itemsNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    if (items.length == 0) {
      return res.apiError(bootle.errors.itemsNotFound.code, { message: bootle.errors.itemsNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    
    // Return the json response.
    res.apiResponse({
      lineStyles: items
    });
    
  });
  
}

/**
 * List LineStyle ids. [ /api/linestyle-ids ]
 */
exports.getids = function(req, res) {
  
  LineStyle.model.find({}, 'slug', function(error, items) {
    
    // Check if an error has occured.
    if (error != null) {
      if (error) {
        var detail = { message: bootle.errors.itemsRetrievalError.message, internalError: { name: error.name, message: error.message } };
        
        return res.apiError(bootle.errors.itemsRetrievalError.code, detail, error, bootle.httpStatus.internalServerError.code);
      }
    }
    
    // Check if items have been found.
    if (!items) {
      return res.apiError(bootle.errors.itemsNotFound.code, { message: bootle.errors.itemsNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    if (items.length == 0) {
      return res.apiError(bootle.errors.itemsNotFound.code, { message: bootle.errors.itemsNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    
    res.apiResponse({
      lineStyleIds: items
    });
    
  });
  
}

/**
 * List LineStyle names. [ /api/linestyle-names ]
 */
exports.getnames = function(req, res) {
  
  LineStyle.model.find({}, 'slug name description', function(error, items) {
    
    // Check if an error has occured.
    if (error != null) {
      if (error) {
        var detail = { message: bootle.errors.itemsRetrievalError.message, internalError: { name: error.name, message: error.message } };
        
        return res.apiError(bootle.errors.itemsRetrievalError.code, detail, error, bootle.httpStatus.internalServerError.code);
      }
    }
    
    // Check if items have been found.
    if (!items) {
      return res.apiError(bootle.errors.itemsNotFound.code, { message: bootle.errors.itemsNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    if (items.length == 0) {
      return res.apiError(bootle.errors.itemsNotFound.code, { message: bootle.errors.itemsNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    
    res.apiResponse({
      lineStyleNames: items
    });
    
  });
  
}

/**
 * Get LineStyle by id. [ /api/linestyle-getbyid/:id ]
 */
exports.getbyid = function(req, res) {
  
  LineStyle.model.findOne({ '_id': req.params.id }, 'slug name description definition', function (error, item) {
    
    // Check if an error has occured.
    if (error != null) {
      if (error) {
        var detail = { message: bootle.errors.itemRetrievalError.message, internalError: { name: error.name, message: error.message } };
        
        return res.apiError(bootle.errors.itemRetrievalError.code, detail, error, bootle.httpStatus.internalServerError.code);
      }
    }
    
    // Check if an item has been found.
    if (!item) {
      return res.apiError(bootle.errors.itemNotFound.code, { message: bootle.errors.itemNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    
    // Return the json response.
    res.apiResponse({
      lineStyle: item
    });
    
  });
  
}

/**
 * Get LineStyle by slug. [ /api/linestyle-getbyslug/:slug ]
 */
exports.getbyslug = function(req, res) {
  
  LineStyle.model.findOne({ 'slug': req.params.slug }, 'slug name description definition', function (error, item) {
    
    // Check if an error has occured.
    if (error != null) {
      if (error) {
        var detail = { message: bootle.errors.itemRetrievalError.message, internalError: { name: error.name, message: error.message } };
        
        return res.apiError(bootle.errors.itemRetrievalError.code, detail, error, bootle.httpStatus.internalServerError.code);
      }
    }
    
    // Check if an item has been found.
    if (!item) {
      return res.apiError(bootle.errors.itemNotFound.code, { message: bootle.errors.itemNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    
    // Return the json response.
    res.apiResponse({
      lineStyle: item
    });
    
  });
  
}

// ================================================================================
