
// ================================================================================
//  University of Manchester. UK.
//  School of Environment, Education, and Development.
//  Centre for Urban Policy Studies.
// 
//  Name:            keyvaluepairs.js [/routes/api]
//  Original coding: Vasilis Vlastaras (@gisvlasta), 04/05/2016.
// 
//  Description:     Defines the functions mapped to the REST API endpoints
//                   to deal with keyvale pairs.
// ================================================================================

var async = require('async');
var keystone = require('keystone');
var bootle = require('../../bootle');

var KeyValuePair = keystone.list('KeyValuePair');

// ================================================================================
//  Public Methods

/**
 * List KeyValuePairs. [ /api/kvs ]
 */
exports.list = function(req, res) {
  
  // Check if a user has been logged in.
  if (!req.user) {
    return res.apiError(bootle.errors.userHasNotLoggedIn.code, { message: bootle.errors.userHasNotLoggedIn.message, internalError: null }, null, bootle.httpStatus.unauthorized.code);
  }
  
  // Check if the user is an administrator.
  if (!req.user.isAdmin) {
    return res.apiError(bootle.errors.userIsNotAnAdministrator.code, { message: bootle.errors.userIsNotAnAdministrator.message, internalError: null } , null, bootle.httpStatus.forbidden.code);
  }
  
  KeyValuePair.model.find(function(error, items) {
    
    // Check if an error has occured.
    if (error != null) {
      if (error) {
        var detail = { message: bootle.errors.itemsRetrievalError.message, internalError: { name: error.name, message: error.message } };
        
        return res.apiError(bootle.errors.itemsRetrievalError.code, detail, error, bootle.httpStatus.internalServerError.code);
      }
    }
    
    // Check if items have been found.
    if (!items) {
      return res.apiError(bootle.errors.itemsNotFound.code, { message: bootle.errors.itemsNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    if (items.length == 0) {
      return res.apiError(bootle.errors.itemsNotFound.code, { message: bootle.errors.itemsNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    
    res.apiResponse({
      keyValuePairs: items
    });
    
  });
}

/**
 * Get KeyValuePair by key. [ /api/kv/:key ]
 */
exports.get = function(req, res) {
  
  // Check if a user has been logged in.
  if (!req.user) {
    return res.apiError(bootle.errors.userHasNotLoggedIn.code, { message: bootle.errors.userHasNotLoggedIn.message, internalError: null }, null, bootle.httpStatus.unauthorized.code);
  }
  
  // Check if the user is an administrator.
  if (!req.user.isAdmin) {
    return res.apiError(bootle.errors.userIsNotAnAdministrator.code, { message: bootle.errors.userIsNotAnAdministrator.message, internalError: null } , null, bootle.httpStatus.forbidden.code);
  }
  
  KeyValuePair.model.findOne({ 'lckey': req.params.key }, 'key value cast', function (error, item) {
    
    // Check if an error has occured.
    if (error != null) {
      if (error) {
        var detail = { message: bootle.errors.itemRetrievalError.message, internalError: { name: error.name, message: error.message } };
        
        return res.apiError(bootle.errors.itemRetrievalError.code, detail, error, bootle.httpStatus.internalServerError.code);
      }
    }
    
    // Check if an item has been found.
    if (!item) {
      return res.apiError(bootle.errors.itemNotFound.code, { message: bootle.errors.itemNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    
    res.apiResponse({
      keyValuePair: item
    });
    
  });
  
}

/**
 * Create a KeyValuePair. [ /api/kv/create ]
 */
exports.create = function(req, res) {
  
  // Check if a user has been logged in.
  if (!req.user) {
    return res.apiError(bootle.errors.userHasNotLoggedIn.code, { message: bootle.errors.userHasNotLoggedIn.message, internalError: null }, null, bootle.httpStatus.unauthorized.code);
  }
  
  // Check if the user is an administrator.
  if (!req.user.isAdmin) {
    return res.apiError(bootle.errors.userIsNotAnAdministrator.code, { message: bootle.errors.userIsNotAnAdministrator.message, internalError: null } , null, bootle.httpStatus.forbidden.code);
  }

  // Return an error if method is not post.
  if (req.method != bootle.httpVerbs.post) {
    return res.apiError(bootle.errors.httpVerbNotAllowed.code, { message: bootle.errors.httpVerbNotAllowed.message, internalError: null }, null, bootle.httpStatus.methodNotAllowed.code);
  };
  
  // Get the data sent through the request body.
  var data = req.body;
  
  // Return an error if data is undefined.
  if (data == undefined) {
    return res.apiError(bootle.errors.undefinedData.code, { message: bootle.errors.undefinedData.message, internalError: null }, null, bootle.httpStatus.badrequest.code);
  };
  
  // Return an error if data is null.
  if (data == null) {
    return res.apiError(bootle.errors.nullData.code, { message: bootle.errors.nullData.message, internalError: null }, null, bootle.httpStatus.badrequest.code);
  };
  
  var item = new KeyValuePair.model(data);
  
  item.save(function(error) {
    res.apiResponse(bootle.success.keyValuePairCreated);
  });
   
}

/**
 * Update KeyValuePair. [ /api/kv/update ]
 */
exports.update = function(req, res) {
  
  // Check if a user has been logged in.
  if (!req.user) {
    return res.apiError(bootle.errors.userHasNotLoggedIn.code, bootle.errors.userHasNotLoggedIn.message, null, bootle.httpStatus.unauthorized.code);
  }
  
  // Check if the user is an administrator.
  if (!req.user.isAdmin) {
    return res.apiError(bootle.errors.userIsNotAnAdministrator.code, bootle.errors.userIsNotAnAdministrator.message, null, bootle.httpStatus.forbidden.code);
  }
  
  // KeyValuePair.model.findById(req.params.id).exec(function(err, item) {
  //   
  //   if (err) return res.apiError('database error', err);
  //   if (!item) return res.apiError('not found');
  //   
  //   var data = (req.method == 'POST') ? req.body : req.query;
  //   
  //   item.getUpdateHandler(req).process(data, function(err) {
  //     
  //     if (err) return res.apiError('create error', err);
  //     
  //     res.apiResponse({
  //       keyvaluepair: item
  //     });
  //     
  //   });
  //   
  // });
  
  // TODO: Needs implementation !!!

  res.apiResponse({
    keyvaluepair: 'k2'
  });
  
}

/**
 * Delete KeyValuePair. [ /api/kv/delete ]
 */
exports.delete = function(req, res) {
  
  // Check if a user has been logged in.
  if (!req.user) {
    return res.apiError(bootle.errors.userHasNotLoggedIn.code, { message: bootle.errors.userHasNotLoggedIn.message, internalError: null }, null, bootle.httpStatus.unauthorized.code);
  }
  
  // Check if the user is an administrator.
  if (!req.user.isAdmin) {
    return res.apiError(bootle.errors.userIsNotAnAdministrator.code, { message: bootle.errors.userIsNotAnAdministrator.message, internalError: null } , null, bootle.httpStatus.forbidden.code);
  }
  
  KeyValuePair.model.findById(req.params.id).exec(function (error, item) {
    
    // Check if an error has occured.
    if (error != null) {
      if (error) {
        var detail = { message: bootle.errors.itemRetrievalError.message, internalError: { name: error.name, message: error.message } };
        
        return res.apiError(bootle.errors.itemRetrievalError.code, detail, error, bootle.httpStatus.internalServerError.code);
      }
    }
    
    // Check if an item has been found.
    if (!item) {
      return res.apiError(bootle.errors.itemNotFound.code, { message: bootle.errors.itemNotFound.message, internalError: null }, null, bootle.httpStatus.notFound.code);
    }
    
    item.remove(function (error) {
      // Check if an error has occured.
      if (error != null) {
        if (error) {
          var detail = { message: bootle.errors.itemDeletionError.message, internalError: { name: error.name, message: error.message } };
          
          return res.apiError(bootle.errors.itemDeletionError.code, detail, error, bootle.httpStatus.internalServerError.code);
        }
      }
      
      
      // TODO: Check if this is better to be: { successCode, id that has been deleted }
      return res.apiResponse({
        success: true
      });
    });
    
  });
  
}

// ================================================================================
